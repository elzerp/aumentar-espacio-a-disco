# Aumentar Espacio a Disco

Script para poder aumentar espacio  en centos.

Si efectuamos una instalación estándar de Centos 7, se darán cuenta de que el volumen root se crea muy pequeño (alrededor de 50GB) 
y el resto del tamaño del disco se la asigna al volumen Home.  Esto puede ser un inconveniente para cuando deseamos tener mas espacio en 
la partición root.  Por tal motivo, hice el siguiente script para aumentar el espacio al disco, espero que les pueda servir.

