#/bin/bash
echo "BIENVENDIDO A HACKING PANAMA"
echo "script para aumentar espacio a disco"
echo "DEBER SER ROOT PARA EJECUTAR EL SCRIPT"


echo "INFORMACION DE LA PARTICION Y VOLUMENOS DE DISCO DURO"
df -hT
echo "CHEQUEAR LA INFORMACION POR DURANTE 30 SEGUNDOS PARA ELEGIR LA PARTICION A BORRAR"
sleep 30

echo "NOS POSICIONAMOS EN RAIZ"
cd /
echo "CREAMOS UNA CARPETA HOME2 PARA BACKUP"
mkdir /home2


echo "backup del home"
cp -r -p /home/* /home2

echo "demontamos el home"
umount /home

echo "Eliminamos el LVM de home"
lvremove /dev/mapper/centos-home

echo "EXTENDER ESPACIO DEL ROOT"
echo "DEFINIR ESPACIO EN GB QUE DESEA EXTENDER"
read a

lvextend -L $aG /dev/mapper/centos-root

sleep 5

echo "Ahora Extendemos el xfs filesystem"
xfs_growfs /dev/mapper/centos-root

echo "CREAMOS EL VOLUMEN NUEVO DE HOME"
echo "DEFINIR EL NUEVO ESPACIO DEL HOME"
read b

lvcreate -L $bg -n home centos

echo "formato XFS para el nuevo home"
mkfs.xfs /dev/mapper/centos-home

echo "montamos el disco del home nuevo"
mount /dev/mapper/centos-home /home


echo "VERIFICACION DEL DISCO HOME NUEVO CON df -hT"
df -hT

echo "FINALMENTE COPIAMOS EL CONTENIDO DEL HOME2 A HOME "

cp -r -p /home2/* /home

echo "HAPPY HACKING"